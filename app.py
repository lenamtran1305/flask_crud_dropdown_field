from flask import Flask, render_template
import requests, json
from dotenv import load_dotenv
import os

load_dotenv()
API_URL = os.environ.get('API_URL')

#
app = Flask(__name__)



def load_data_from_api(method, url):
    payload = {}
    headers = {}

    response = requests.request(method, url, headers=headers, data=payload)

    data = json.loads(response.text)
    return data


@app.route('/')
def index():
    v = 'Doanh nghiệp'
    hs = load_data_from_api(method='GET', url=API_URL)

    return render_template('index.html', **locals())


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
